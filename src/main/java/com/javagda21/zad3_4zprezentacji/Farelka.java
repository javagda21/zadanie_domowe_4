package com.javagda21.zad3_4zprezentacji;

public class Farelka implements Grzeje {
    private double ustawionaTemperatura = 20.0;


    public double pobierzTemp() {
        return ustawionaTemperatura;
    }

    public void zwiekszTemp() {
        ustawionaTemperatura++;
    }
}
