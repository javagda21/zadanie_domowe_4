package com.javagda21.zad3_4zprezentacji;

public class Wiatrak implements Chlodzi {
    private double ustawionaTemperatura = 20.0;

    public double pobierzTemp() {
        return ustawionaTemperatura;
    }

    public void schlodz() {
        ustawionaTemperatura--;
    }
}
