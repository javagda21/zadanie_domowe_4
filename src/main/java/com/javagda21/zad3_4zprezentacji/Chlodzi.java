package com.javagda21.zad3_4zprezentacji;

public interface Chlodzi {
    double pobierzTemp();
    void schlodz();

    default void wyswietlTemperature(){
        System.out.println("Aktualna temperatura w pomieszczeniu wynosi " + pobierzTemp() + " stopni Celsjusza.");
    }
}
