package com.javagda21.zad3_4zprezentacji;

public class Klimatyzacja implements Grzeje, Chlodzi {
    private double ustawionaTemperatura = 20.0;


    public void schlodz() {
        ustawionaTemperatura--;
    }

    public double pobierzTemp() {
        return ustawionaTemperatura;
    }

    public void zwiekszTemp() {
        ustawionaTemperatura++;
    }

    @Override
    public void wyswietlTemperature() {
        Grzeje.super.wyswietlTemperature();
    }
}
