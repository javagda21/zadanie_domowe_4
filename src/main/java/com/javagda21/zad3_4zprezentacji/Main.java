package com.javagda21.zad3_4zprezentacji;

public class Main {
    public static void main(String[] args) {
        Farelka farelka = new Farelka();
        System.out.println(farelka.pobierzTemp());
        farelka.wyswietlTemperature();
        farelka.zwiekszTemp();
        farelka.wyswietlTemperature();

        System.out.println();

        Wiatrak wiatrak = new Wiatrak();
        System.out.println(wiatrak.pobierzTemp());
        wiatrak.wyswietlTemperature();
        wiatrak.schlodz();
        wiatrak.wyswietlTemperature();

        System.out.println();

        Klimatyzacja klimatyzacja = new Klimatyzacja();
        klimatyzacja.wyswietlTemperature();
        klimatyzacja.schlodz();
        klimatyzacja.wyswietlTemperature();
        klimatyzacja.zwiekszTemp();
        klimatyzacja.wyswietlTemperature();

    }
}
