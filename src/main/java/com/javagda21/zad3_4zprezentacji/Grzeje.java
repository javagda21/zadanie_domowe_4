package com.javagda21.zad3_4zprezentacji;

public interface Grzeje {
    double pobierzTemp();
    void zwiekszTemp();

    default void wyswietlTemperature(){
        System.out.println("Aktualna temperatura w pomieszczeniu wynosi " + pobierzTemp() + " stopni Celsjusza.");
    }
}
