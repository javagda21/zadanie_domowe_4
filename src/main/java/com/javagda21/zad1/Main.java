package com.javagda21.zad1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Robot robot = new Robot("Marian");

        String komenda;

        do {
            System.out.println("Podaj komende:");
            komenda = scanner.nextLine();

            String[] slowa = komenda.split(" ");

            if (slowa[0].equalsIgnoreCase("ruch")) {
                String jakiRuch = slowa[1];

                try {
                    RuchRobota ruch = RuchRobota.valueOf(jakiRuch.toUpperCase().trim());

                    robot.poruszRobotem(ruch);
                } catch (IllegalArgumentException iae) {
                    System.err.println("Niepoprawny typ ruchu!");
                }
            }
            if (slowa[0].equalsIgnoreCase("naladuj")) {
                robot.naładujRobota();
            }

            if (slowa[0].equalsIgnoreCase("wlacz")) {
                robot.włączRobota();
            }

            if (slowa[0].equalsIgnoreCase("wylacz")) {
                robot.wyłączRobota();
            }

        } while (!komenda.equalsIgnoreCase("quit"));
    }
}
