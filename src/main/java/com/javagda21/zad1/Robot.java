package com.javagda21.zad1;

public class Robot {
    private int poziomBaterii = 100;
    private String nazwaRobota;
    private boolean czyWlaczony = false;

    public Robot(String nazwaRobota) {
        this.nazwaRobota = nazwaRobota;
    }

    public void poruszRobotem(RuchRobota ruchRobota) {
        if (czyWlaczony) {
            // możemy wykonać ruch
            if(poziomBaterii >= ruchRobota.getPotrzebnaMoc()) {
                poziomBaterii -= ruchRobota.getPotrzebnaMoc();
                System.out.println("Wykonuję ruch: " + ruchRobota);
            }else{
                System.err.println("Mocy przybywaj.. :( ni ma");
            }
        } else {
            System.err.println("Robot musi być włączony.");
        }
    }

    public void naładujRobota(){
        if(!czyWlaczony){
            poziomBaterii = 100;
            System.out.println("Poziom baterii uzupełniony.");
        }else{
            System.out.println("Wyłącz robota");
        }
    }

    public void włączRobota(){
        if(!czyWlaczony){
            czyWlaczony = true;
            System.out.println("Robot włączony");
        }else{
            System.out.println("Robot jest już włączony.");
        }
    }

    public void wyłączRobota(){
        if(czyWlaczony){
            czyWlaczony = false;
            System.out.println("Robot wyłączony");
        }else{
            System.out.println("Robot jest już wyłączony.");
        }
    }

}
