package com.javagda21.zad2;

import java.util.Arrays;

public class Miasto {
    private Obywatel[] obywatele;


    public Miasto(Obywatel[] obywatele) {
        this.obywatele = obywatele;
    }

//    public void setObywatele(Obywatel[] obywatele) {
//        this.obywatele = obywatele;
//    }

    public int iluObywateliMozeGlosowac() {
        int licznik = 0;
        for (Obywatel obywatel : obywatele) {
            if (obywatel.mozeGlosowac()) {
                licznik++;
            }
        }
        return licznik;
    }

    public Obywatel[] ktoMozeGlosowac(){
        Obywatel[] ciCoMogaGlosowac = new Obywatel[iluObywateliMozeGlosowac()];

        int licznikWstawiania = 0;
        for (int i = 0; i < obywatele.length; i++) {
            if(obywatele[i].mozeGlosowac()){
                ciCoMogaGlosowac[licznikWstawiania++] = obywatele[i];
            }
        }

        return ciCoMogaGlosowac;
    }
}
