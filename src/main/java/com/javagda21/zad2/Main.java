package com.javagda21.zad2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Obywatel[] obywatels = new Obywatel[14];
        obywatels[0] = new Król("a");
        obywatels[1] = new Żołnierz("b");
        obywatels[2] = new Żołnierz("c");
        obywatels[3] = new Żołnierz("d");
        obywatels[4] = new Żołnierz("e");
        obywatels[5] = new Żołnierz("f");
        obywatels[6] = new Mieszczanin("g");
        obywatels[7] = new Mieszczanin("h");
        obywatels[8] = new Mieszczanin("i");
        obywatels[9] = new Mieszczanin("j");
        obywatels[10] = new Chłop("k");
        obywatels[11] = new Chłop("l");
        obywatels[12] = new Chłop("m");
        obywatels[13] = new Chłop("n");

        Miasto town = new Miasto(obywatels);
        int ilu = town.iluObywateliMozeGlosowac();
        System.out.println(ilu + " może głosować.");

        Obywatel[] obywateleZdolniDoGlosowania = town.ktoMozeGlosowac();
        System.out.println("Mogą głosować: " + Arrays.toString(obywateleZdolniDoGlosowania));

//        Obywatel[] obywatels2 = new Obywatel[3];
//        obywatels2[0] = new Król();
//        obywatels2[1] = new Król();
//        obywatels2[2] = new Król();
//        town.setObywatele(obywatels2);


    }
}
