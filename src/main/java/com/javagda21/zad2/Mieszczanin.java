package com.javagda21.zad2;

public class Mieszczanin extends Obywatel {
    public Mieszczanin(String imie) {
        super(imie);
    }

    public boolean mozeGlosowac() {
        return true;
    }

    @Override
    public String toString() {
        return "Mieszczanin{" +
                "imie='" + imie + '\'' +
                '}';
    }
}
