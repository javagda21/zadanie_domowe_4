package com.javagda21.zad2;

public class Król extends Obywatel {
    public Król(String imie) {
        super(imie);
    }

    public boolean mozeGlosowac() {
        return false;
    }

    @Override
    public String toString() {
        return "Król{" +
                "imie='" + imie + '\'' +
                '}';
    }
}
