package com.javagda21.zad2;

public abstract class Obywatel {
    protected String imie;

    public Obywatel(String imie) {
        this.imie = imie;
    }

    public abstract boolean mozeGlosowac();

    @Override
    public String toString() {
        return "Obywatel{" +
                "imie='" + imie + '\'' +
                '}';
    }
}
